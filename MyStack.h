//
// Created by ivametal on 23/04/17.
//

#ifndef VADIMLOH_MYSTACK_H
#define VADIMLOH_MYSTACK_H


#include "Value.h"

class MyStack {
private :
    int currentIndex;
    Value* currentValue;

public :
    MyStack();
    ~MyStack();

    void push(int val);

    int pop();

    Value* getLastValuePointer();
};


#endif //VADIMLOH_MYSTACK_H
