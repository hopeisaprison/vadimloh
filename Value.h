//
// Created by ivametal on 23/04/17.
//

#ifndef VADIMLOH_VALUE_H
#define VADIMLOH_VALUE_H


class Value {
private:
    int value;
    Value* previousValue;

public :

    Value(int value);

    int getValue();

    void setPrevious(Value* previous);

    Value* getPrevious();

};


#endif //VADIMLOH_VALUE_H
