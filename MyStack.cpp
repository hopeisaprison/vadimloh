//
// Created by ivametal on 23/04/17.
//

#include <stddef.h>
#include <iostream>
#include "MyStack.h"

MyStack::MyStack() {
    currentIndex = 0;
}

MyStack ::~MyStack() {
    delete currentValue;
}

void MyStack::push(int val) {
    Value* value = new Value(val);
    if (currentIndex==0) {
        value->setPrevious(nullptr);
        this->currentValue = value;
        currentIndex++;
    }
    else {
        value->setPrevious(this->currentValue);
        this->currentValue = value;
        currentIndex++;
    }
}

int MyStack::pop() {
    int returnValue = this->currentValue->getValue();
    currentValue = currentValue->getPrevious();
    currentIndex--;
    return returnValue;
}

Value* MyStack::getLastValuePointer() {
    return currentValue;
}
