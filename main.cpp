#include <iostream>
#include "MyStack.h"


int main() {
    MyStack *stackWithNumbers = new MyStack();
    MyStack *stackWithEvenNumbers = new MyStack();
    MyStack *stackWithNotEvenNumbers = new MyStack();

    for (int i = 0; i <= 19; i++) {
        stackWithNumbers->push(i);
    }

    for (int i = 0; i <= 19; i++) {
        int currentStackValue = stackWithNumbers->pop();
        if (currentStackValue % 2 == 0){
            stackWithEvenNumbers->push(currentStackValue);
            }
        else
            stackWithNotEvenNumbers->push(currentStackValue);
    }


    std :: cout << stackWithNotEvenNumbers->getLastValuePointer()<< " "<< stackWithEvenNumbers->getLastValuePointer();
}
